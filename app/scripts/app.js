'use strict';

angular
  .module('rainbowhamburgerApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/shell.html',
        controller: 'ShellController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
